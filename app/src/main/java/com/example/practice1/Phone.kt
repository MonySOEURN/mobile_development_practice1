package com.example.practice1

data class Phone(
    val id: Int,
    val name: String,
    val price: Int,
    val brandId: Int,
    val imageUrl: String
) {
}