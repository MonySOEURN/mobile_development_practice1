package com.example.practice1

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.phone_view_holder.view.*
import com.bumptech.glide.request.RequestOptions



class PhoneAdapter : RecyclerView.Adapter<PhoneViewholder>() {

    var phones = arrayListOf<Phone>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhoneViewholder {
        var layoutInflater = LayoutInflater.from(parent.context)
        var itemView = layoutInflater.inflate(R.layout.phone_view_holder, parent, false)
        return PhoneViewholder(itemView)
    }

    override fun getItemCount(): Int {
        return phones.size
    }

    override fun onBindViewHolder(holder: PhoneViewholder, position: Int) {
        val phone = phones[position]
        holder.itemView.txtName.text = phone.name
        holder.itemView.txtPrice.text = "\$ ${phone.price}"
        Glide.with(holder.itemView)  //2
            .load(phone.imageUrl) //3
            .centerCrop() //4
            .apply(RequestOptions().override(500, 750))
            .into(holder.itemView.imgPhone)
    }
}

class PhoneViewholder(itemView: View) : RecyclerView.ViewHolder(itemView){

}