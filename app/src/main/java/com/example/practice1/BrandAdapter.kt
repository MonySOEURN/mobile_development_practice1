package com.example.practice1

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.brand_view_holder.view.*

class BrandAdapter : RecyclerView.Adapter<BrandViewHolder>() {

    var brands = arrayListOf<Brand>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BrandViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(R.layout.brand_view_holder, parent, false)
        return BrandViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return brands.size
    }

    override fun onBindViewHolder(holder: BrandViewHolder, position: Int) {
        val brand = brands[position]
        holder.itemView.txtBrand.text = brand.name
    }
}

class BrandViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){}