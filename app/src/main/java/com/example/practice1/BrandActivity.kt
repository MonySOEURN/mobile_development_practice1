package com.example.practice1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_brand.*

class BrandActivity : AppCompatActivity() {

    lateinit var brandAdapter: BrandAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_brand)

        brandRecycleView.layoutManager = LinearLayoutManager(this)
        brandAdapter = BrandAdapter()
        brandRecycleView.adapter = brandAdapter

        loadBrandFromServer()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun loadBrandFromServer(){
        val brandUrl = "http://lp.js-cambodia.com/rupp/brands.php"

        val branQue = Volley.newRequestQueue(this)

        val brandRequest = JsonArrayRequest(brandUrl,
            Response.Listener {
                val brands = arrayListOf<Brand>()
                for (i in 0 until it.length()){
                    val brandJson = it.getJSONObject(i)
                    val id = brandJson.getInt("id")
                    val name = brandJson.getString("name")

                    val brand = Brand(id, name)
                    brands.add(brand)
                }
                brandAdapter.brands = brands
                brandAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener {
                Toast.makeText(this, "load data failed", Toast.LENGTH_LONG)
                Log.d("fe", "load data failed "+ it.message )
            })
        branQue.add(brandRequest)
    }
}
