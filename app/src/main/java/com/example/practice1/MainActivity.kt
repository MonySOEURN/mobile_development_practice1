package com.example.practice1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var adapter: PhoneAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val btnOpenActivity = findViewById<Button>(R.id.btnBrand)
        btnOpenActivity.setOnClickListener {
            startActivity(Intent(this@MainActivity, BrandActivity::class.java))
        }

        recyclerView.layoutManager = LinearLayoutManager(this)

        loadPhoneFromServer()
        adapter = PhoneAdapter()
        recyclerView.adapter = adapter

    }

    fun loadPhoneFromServer(){

        /***
         * step to for using volley
         * 1. Initialize Request Que
         * 2. Store Request response from URL
         * 3. Add response to Que
         */

        val url = "http://lp.js-cambodia.com/rupp/phones.php"
        val que = Volley.newRequestQueue(this)

        val request = JsonArrayRequest(url, Response.Listener {
            val phones = arrayListOf<Phone>()
            for (i in 0 until it.length()){
                val phoneJson = it.getJSONObject(i)
                val name = phoneJson.getString("name")
                val id = phoneJson.getInt("id")
                val brandId = phoneJson.getInt("brandId")
                val price = phoneJson.getInt("price")
                val imageUrl = phoneJson.getString("imageUrl")

                val phone = Phone( id, name, price, brandId, imageUrl)
                phones.add(phone)
            }
            adapter.phones = phones
            adapter.notifyDataSetChanged()
        }, Response.ErrorListener {
            Toast.makeText(this, "load data error", Toast.LENGTH_LONG).show()
            Log.d("fe", "load data Error "+ it.message)
        })

        que.add(request)
    }
}
